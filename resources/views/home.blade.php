<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Aula de Laravel</title>
</head>
<body>
    <nav>
        <ul>
            <li><a href={{route('home')}}>Home</a></li>
            <li><a href="{{route('empresa')}}">Empresa</a></li>
            <li><a href={{route('servico')}}>Serviço</a></li>
            <li><a href={{route('contato')}}>Contato</a></li>
        </ul>
    </nav>

    <h1>HOME</h1>
    <p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.</p>
</body>
</html>