<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Aula de Laravel</title>
</head>
<body>
    <nav>
        <ul>
            <li><a href={{route('home')}}>Home</a></li>
            <li><a href="{{route('empresa')}}">Empresa</a></li>
            <li><a href={{route('servico')}}>Serviço</a></li>
            <li><a href={{route('contato')}}>Contato</a></li>
        </ul>
    </nav>

    <h1>CONTATO</h1>
    <p>O trecho padrão original de Lorem Ipsum, usado desde o século XVI, está reproduzido abaixo para os interessados. Seções 1.10.32 e 1.10.33 de "de Finibus Bonorum et Malorum" de Cicero também foram reproduzidas abaixo em sua forma exata original, acompanhada das versões para o inglês da tradução feita por H. Rackham em 1914.</p>
    <form action="/pagina-processa-dados-do-form" method="post">
        <div>
            <label for="name">Nome:</label>
            <input type="text" />
        </div>

        <div>
            <label for="mail">E-mail:</label>
            <input type="email"/>
        </div>

        <div>
            <label for="msg">Mensagem:</label>
            <textarea ></textarea>
        </div>

        <div class="button">
            <button type="submit">Enviar</button>
        </div>

    </form>
</body>
</html>